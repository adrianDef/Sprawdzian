package com.javaTraining.model;

import java.util.List;

/**
 * Created by Adi on 2017-04-18.
 */
public class Question {
    private int id;
    private String question;
    private List<String> answers;
    private int correctAnswer;
    private boolean isQuestionBoolean;

    public Question(int id, String question,
                    List<String> answers, int correctAnswer, boolean isQuestionBoolean) {
        this.id = id;
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
        this.isQuestionBoolean = isQuestionBoolean;
    }

    public Question(Question question) {
        this.id = question.getId();
        this.question = question.getQuestion();
        this.answers = question.getAnswers();
        this.correctAnswer = question.getCorrectAnswer();
        this.isQuestionBoolean = question.isQuestionBoolean();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public boolean isQuestionBoolean() {
        return isQuestionBoolean;
    }
}
