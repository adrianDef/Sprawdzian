package com.javaTraining.dao;

import com.javaTraining.model.Question;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Test {
    private String FILE_NAME;

    private List<Question> questions;
    private int correctAnswerCounter;


    public Test() {
        this.questions = new ArrayList<>();
        this.correctAnswerCounter = 0;

        File[] files = new File("Tests/").listFiles();
        System.out.println("UTWORZONO NOWY TEST. " +
                "WYBIERZ PLIK ZE SPRAWDZIANEM:");

        Arrays.stream(files)
                .forEach(f -> System.out.println(f.getName()));
        Scanner scanner = new Scanner(System.in);
        this.FILE_NAME = scanner.nextLine();
        try {
            Files.lines(Paths.get("Tests/" + FILE_NAME))
                    .forEach(l -> questions.add(createQuestionFromFileLine(l)));
        } catch (IOException e) {
            throw new RuntimeException("Błąd odczytu pliku źródłowego");
        }
    }

    public void start() {
        System.out.println("TEST Z WIEDZY OGÓLNEJ:\n\n");
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < questions.size(); i++) {
            System.out.println("PYTANIE " + (i + 1) + ":\n" +
                    getQuestion(i));
            if (questions.get(i).isQuestionBoolean()) {
                boolean isAnswerTypeCorrect = false;
                int intAnswer = 0;
                while (!isAnswerTypeCorrect) {
                    System.out.println("ODPOWIEDŹ: ");
                    String answer = scanner.nextLine().toLowerCase();
                    switch (answer) {
                        case "t":
                            intAnswer = 0;
                            isAnswerTypeCorrect = true;
                            break;
                        case "tak":
                            intAnswer = 0;
                            isAnswerTypeCorrect = true;
                            break;
                        case "n":
                            intAnswer = 1;
                            isAnswerTypeCorrect = true;
                            break;
                        case "nie":
                            intAnswer = 1;
                            isAnswerTypeCorrect = true;
                            break;
                        default:
                            System.out.println("Błędny typ odpowiedzi -" +
                                    " wpisz tak/nie!");
                            break;
                    }
                }

                if (intAnswer == questions.get(i).getCorrectAnswer()) {
                    System.out.println("Poprawna odpowiedź\n");
                    correctAnswerCounter++;
                } else {
                    System.out.println("Błąd! poprawna odpowiedź to " +
                            getCorrectAnswer(i) + "\n");
                }
            } else {
                getAnswers(i);
                System.out.println("ODPOWIEDŹ: ");
                int answer = scanner.nextInt() - 1;
                scanner.nextLine();
                if (answer == questions.get(i).getCorrectAnswer()) {
                    System.out.println("Poprawna odpowiedź\n");
                    correctAnswerCounter++;
                } else {
                    System.out.println("Błąd! poprawna odpowiedź to " +
                            getCorrectAnswer(i) + "\n");
                }
            }
        }
        System.out.println("\n\nTwój wynik to: " + correctAnswerCounter
                + "/" + questions.size());
    }

    private String getQuestion(int id) {
        return questions.stream().filter(q -> q.getId() == id)
                .map(com.javaTraining.model.Question::getQuestion)
                .findFirst().orElse("Index out of range!");
    }

    private Question get(int id) {
        return questions.stream().filter(q -> q.getId() == id)
                .findFirst().orElse(null);
    }

    private void getAnswers(int id) {
        for (int i = 0; i < questions.get(id).getAnswers().size(); i++) {
            System.out.println(i + 1 + ". " + getAnswerList(get(id)).get(i));
        }
    }

    private List<String> getAnswerList(Question question) {
        return question.getAnswers();
    }

    private String getCorrectAnswer(int index) {
        Question question = questions.get(index);
        return getAnswerList(question).get(question.getCorrectAnswer());
    }

    public List<String> getAll() {
        return questions.stream()
                .map(Question::getQuestion)
                .collect(Collectors.toList());
    }

    public void add(Question question) {
        question.setId(getNextId());
        questions.add(new Question(question));
        saveToFile();
    }

    private void saveToFile() {
        List<String> fileLines = questions.stream()
                .map(this::createFileLineFromQuestion)
                .collect(Collectors.toList());
        try {
            Files.write(Paths.get("Tests/" + FILE_NAME),
                    fileLines,
                    StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Błąd zapisu do pliku");
        }
    }

    private String createFileLineFromQuestion(Question question) {
        StringBuilder answers = new StringBuilder();
        for (String answer : question.getAnswers()) {
            answers.append(answer).append(",");
        }

        return question.getId() + ","
                + question.getQuestion() + ","
                + answers.toString() +
                +question.getCorrectAnswer();
    }

    private int getNextId() {
        return questions.stream()
                .mapToInt(com.javaTraining.model.Question::getId).max()
                .orElse(0) + 1;
    }

    private Question createQuestionFromFileLine(String fileLine) {
        String[] lineValues = fileLine.split(",");
        List<String> answers = new ArrayList<>();
        int id = Integer.parseInt(lineValues[0]);
        String question = lineValues[1];
        for (int i = 2; i < lineValues.length - 2; i++) {
            answers.add(lineValues[i]);
        }
        int correctAnswer = Integer.parseInt(lineValues[lineValues.length - 2]);
        boolean isQuestionBoolean = Boolean.parseBoolean(lineValues[lineValues.length - 1]);

        return new Question(id, question, answers, correctAnswer, isQuestionBoolean);
    }


}
